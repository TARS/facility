from setuptools import setup, find_packages


setup(
    name='facility',
    packages=find_packages(),
    install_requires=[
        'Django==4.0.5',
        'django-bootstrap4==22.1',
    ]
)
