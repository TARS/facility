from django.db import models
from django.utils.translation import gettext as _


class Location(models.Model):
    name = models.CharField(max_length=200, verbose_name=_('location'))
    identifier = models.CharField(max_length=50, null=True, blank=True, default=None,
                                  verbose_name=_('identifier'))
    environment = models.ForeignKey("Location", on_delete=models.CASCADE,
                                    null=True, blank=True, default=None,
                                    verbose_name=_('environment'))

    class Meta:
        verbose_name = _('location')
        verbose_name_plural = _('locations')

    def __str__(self):
        if self.environment is not None:
            return str(self.environment) + " / " + str(self.name)
        return self.name


class ServicePartner(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(null=True, blank=True, default=None)
    phone = models.CharField(max_length=30, null=True, blank=True, default=None)
    address = models.TextField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = _('service partner')
        verbose_name_plural = _('service partners')

    def __str__(self):
        return self.name


class ServiceRoutine(models.Model):
    PERIOD_CHOICES = (('y', _('yearly')),
                      ('q', _('quarterly')),
                      ('m', _('monthly')))
    name = models.CharField(max_length=200)
    review_period = models.CharField(max_length=1, choices=PERIOD_CHOICES)

    class Meta:
        verbose_name = _('service routine')
        verbose_name_plural = _('service routines')

    def __str__(self):
        return self.name


class ServiceTask(models.Model):
    name = models.CharField(max_length=200)
    service_routine = models.ForeignKey("ServiceRoutine", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('service task')
        verbose_name_plural = _('service tasks')

    def __str__(self):
        return self.name


class ServiceQuestion(models.Model):
    question = models.CharField(max_length=300)
    service_task: ServiceTask = models.ForeignKey('ServiceTask', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('service question')
        verbose_name_plural = _('service questions')

    def __str__(self):
        return "{}, {}: {}".format(self.service_task.service_routine.name,
                                   self.service_task.name, self.question)


class ServiceRun(models.Model):
    service_routine = models.ForeignKey("ServiceRoutine", on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('service run')
        verbose_name_plural = _('service runs')

    def __str__(self):
        return str(self.service_routine)


class ServiceResult(models.Model):
    service_run = models.ForeignKey('ServiceRun', on_delete=models.CASCADE)
    question = models.ForeignKey('ServiceQuestion', on_delete=models.CASCADE)
    result = models.BooleanField(default=False)
    notice = models.CharField(max_length=200, null=True, blank=True, default=None)

    class Meta:
        verbose_name = _('service result')
        verbose_name_plural = _('service results')

    def __str__(self):
        result = "{}: ".format(self.question)
        if self.result:
            result += _('Passed')
        else:
            result = _('Failed')
        if self.notice is not None:
            result += " ({})".format(self.notice)
        return result


class ExternalService(models.Model):
    name = models.CharField(max_length=200)
    reason = models.TextField(null=True, blank=True, default=None)
    rma = models.CharField(max_length=200, null=True, blank=True, default=None)
    device = models.ForeignKey("Equipment", on_delete=models.CASCADE)
    service_partner = models.ForeignKey("ServicePartner", on_delete=models.CASCADE)
    dispatch_date = models.DateField(null=True, blank=True, default=None)
    return_date = models.DateField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = _('service')
        verbose_name_plural = _('external services')

    def __str__(self):
        return self.name


class Equipment(models.Model):
    name = models.CharField(max_length=200)
    identifier = models.CharField(max_length=50, null=True, blank=True, default=None)
    environment = models.ForeignKey("Location", on_delete=models.CASCADE)
    manufacturer = models.CharField(max_length=200, null=True, blank=True, default=None)
    production_date = models.DateField(null=True, blank=True, default=None)
    withdrawal_date = models.DateField(null=True, blank=True, default=None)
    info = models.TextField(null=True, blank=True, default=None)
    service_partners = models.ManyToManyField('ServicePartner', blank=True)

    class Meta:
        verbose_name = _('equipment')
        verbose_name_plural = _('equipments')

    def __str__(self):
        return "{} ({})".format(self.name, self.environment)
