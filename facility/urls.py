from django.urls import path
from django.contrib import admin as django_admin
from facility import views
from facility import admin

urlpatterns = [
    path('', views.index),
    path('admin/', django_admin.site.urls),
]
