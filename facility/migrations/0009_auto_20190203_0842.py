# Generated by Django 2.1.5 on 2019-02-03 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facility', '0008_auto_20190203_0812'),
    ]

    operations = [
        migrations.AddField(
            model_name='externalservice',
            name='dispatch_date',
            field=models.DateField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='externalservice',
            name='retun_date',
            field=models.DateField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='serviceroutine',
            name='review_period',
            field=models.CharField(choices=[('y', 'yearly'), ('q', 'quarterly'), ('m', 'monthly')], default='y', max_length=1),
            preserve_default=False,
        ),
    ]
