$('#barcodeModal').on('show.bs.modal', function() {
	Quagga.init({
		inputStream: {
			type:"LiveStream",
			constraints: {
				width: {min: 640},
				height: {min: 480},
				aspectRatio: {
					min: 1,
					max: 100
				},
				facingMode: "environment"
			},
			target: document.querySelector('#barcodeTarget'),
		},
		locator: {
			patchSize: "medium",
			halfSample: true
		},
		numOfWorkers: 2,
		frequency: 10,
		decoder: {
			readers: [
				{format:"ean_reader", config:{}}
			]
		},
		locate:true
	}, function(err) {
		if (err) {
			console.log(err);
			return
		}
		console.log("Initialization finished. Ready to start");
	});
});

$('#barcodeModal').on('hidden.bs.modal', function() {
	Quagga.stop();
});

Quagga.onProcessed(function(result) {
	var drawingCtx = Quagga.canvas.ctx.overlay,
	    drawingCanvas = Quagga.canvas.dom.overlay;

	if (result) {
		if (result.boxes) {
			drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")),
					     parseInt(drawingCanvas.getAttribute("height")));
			result.boxes.filter(function (box) {
				return box !== result.box;
			}).forEach(function (box) {
				Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, 
							   {color: "green", lineWidth: 2});
			});
		}

		if (result.box) {
			Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, 
						   {color: "#00F", lineWidth: 2});
		}

		if (result.codeResult && result.codeResult.code) {
			Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, 
						   {color: 'red', lineWidth: 3});
		}
	}
});
