from django.contrib import admin
from django.utils.safestring import mark_safe
from django.urls import reverse

from .models import *


class ServiceQuestionInline(admin.StackedInline):
    model = ServiceQuestion
    extra = 0


class ServiceTaskAdmin(admin.ModelAdmin):
    inlines = [ServiceQuestionInline, ]


class ServiceTaskInline(admin.StackedInline):
    model = ServiceTask
    extra = 0
    show_change_link = True


class ServiceRoutineAdmin(admin.ModelAdmin):
    inlines = [ServiceTaskInline, ]


admin.site.register(Location)
admin.site.register(ServicePartner)
admin.site.register(ServiceRoutine, ServiceRoutineAdmin)
admin.site.register(ServiceTask, ServiceTaskAdmin)
admin.site.register(ServiceQuestion)
admin.site.register(ServiceRun)
admin.site.register(ServiceResult)
admin.site.register(ExternalService)
admin.site.register(Equipment)
