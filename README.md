# Django based facility management

The facility management of a public aid agency like a voluntary fire brigade or civil protection is a 
repetitive task. Assets have to be checked a periodic cycles. This software aims to support the 
equipment manager in charge to comply with the terms and to keep track of finished and outstanding 
tasks.

## Setup

Install through pip. All dependencies should be installed automatically.

## Setup database

```bash
python3 manage.py migrate
```

## Create admin user

```bash
python3 manage.py createsuperuser
```

## Run development server

```bash
python3 manage.py runserver
```

## Classes

Our starting point were the existing paper based check lists called "Werkstattzettel". For each 
_Equipment_ asset there is a _ServiceRoutine_ defined. Each _ServiceRoutine_ defines different 
_ServiceTasks_ like visual or functional checks. Each of these _ServiceTasks_ requires several 
_ServiceQuestions_ to be considered. These Classes define the tasks which need to be performed
periodically.

Every time such a _ServiceRoutine_ is performed a new _ServiceRun_ is started. There all _ServiceQuestions_ need to be checked and the results are saved as _ServiceResults_. 

To allow efficient workflow each _Equipment_ asset is associated to a _Location_ describing where to find the asset and _ServicePartners_ which provide service for the asset if professional service like technical check or repair is required. In that case an _ExternalService_ can be created to keep track of devices which are temporarily out of service.

